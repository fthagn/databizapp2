import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { HttpClientModule } from '@angular/common/http';

// import { Firebase } from '@ionic-native/firebase/ngx';

const config = {
    apiKey: 'AIzaSyAhOf77V9FTZd6Xu9WRMoujQWSbiIHL500',
    authDomain: 'databiz-58eb5.firebaseapp.com',
    databaseURL: 'https://databiz-58eb5.firebaseio.com',
    projectId: 'databiz-58eb5',
    storageBucket: 'databiz-58eb5.appspot.com',
    messagingSenderId: '119549211216',
    appId: '1:119549211216:web:31c9d7a1d24e1b70'
  };

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    HttpClientModule,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    // Firebase,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
